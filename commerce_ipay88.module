<?php

/**
 * @file
 * Implements IPay88 payment services for use with Drupal Commerce.
 */

/**
 * Implements hook_init().
 */
function commerce_ipay88_init() {
  if (module_exists('libraries')) {
    // Include IPay88 class.
    $path = libraries_get_path('ipay88');
    if ($path) {
      include_once $path . '/IPay88.class.php';
    }
  }
}

/**
 * Implements hook_permission().
 */
function commerce_ipay88_permission() {
  return array(
    'administer commerce ipay88' => array(
      'title' => t('Administer Commerce IPay88'),
      'description' => t('Perform administration tasks for Commerce IPay88.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function commerce_ipay88_menu() {
  $items = array();

  $items['admin/commerce/config/ipay88'] = array(
    'title' => 'IPay88 settings',
    'description' => t('Configure IPay88 settings.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_ipay88_admin_form'),
    'access arguments' => array('administer commerce ipay88'),
  );

  return $items;
}

/**
 * Implements hook_cron().
 */
function commerce_ipay88_cron() {
  // Re-query for status.
  // Fetch ipay88 transaction that are not 'requeried' yet.
  if (variable_get('commerce_ipay88_cron_requery_enabled', FALSE)) {
    $ipay88 = new IPay88();
    if (variable_get('commerce_ipay88_cron_requery_limit') == 0) {
      // No limit requery.
      $query = db_query('SELECT ci.merchant_code, ci.ref_no, ci.amount, ci.currency FROM {commerce_ipay88} ci WHERE ci.requery_status = 0 AND ci.requery_timestamp = 0');
    }
    else {
      $query = db_query('SELECT ci.merchant_code, ci.ref_no, ci.amount, ci.currency FROM {commerce_ipay88} ci WHERE ci.requery_status = 0 AND ci.requery_timestamp = 0 LIMIT :limit', array(':limit' => variable_get('commerce_ipay88_cron_requery_limit', 50)));
    }
    $result = $query->fetchAll();
    if ($result) {
      foreach ($result as $record) {
         $status = $ipay88->requery(array('MerchantCode' => $record->merchant_code, 'RefNo' => $record->ref_no, 'Amount' => number_format(commerce_currency_amount_to_decimal($record->amount, strtoupper($record->currency)), 2)));

          // Now we update the status.
          $updated = db_update('commerce_ipay88')
            ->fields(array(
              'requery_status' => ($status == '00') ? 1 : 0,
              'requery_timestamp' => REQUEST_TIME,
            ))
            ->condition('ref_no', $record->ref_no, '=')
            ->execute();

          if ($updated) {
            watchdog('commerce_ipay88', t('Commerce IPay88 re-query update was successful.'));
          }
          else {
            watchdog('commerce_ipay88', t('Commerce IPay88 re-query failed to update record.'), 'error');
          }
      }
    }
  }
}

/**
 * Commerce IPay88 admin form.
 *
 * @todo Add configuration to select available payment methods and currency.
 */
function commerce_ipay88_admin_form($form, &$form_state) {
  $form['merchant'] = array(
    '#type' => 'fieldset',
    '#title' => t('Merchant information'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['merchant']['ipay88_logo'] = array(
    '#markup' => l(
      theme_image(array(
        'path' => drupal_get_path('module', 'commerce_ipay88') . '/images/ipay88-logo.png',
        'alt' => t('IPay88 - Internet Payment by Mobile88.com.'),
        'title' => t('IPay88 - Internet Payment by Mobile88.com.'),
        'attributes' => array('style' => 'float: right; margin: 0 1em 1em 0;'),
      )),
      'http://www.ipay88.com',
      array('html' => TRUE, 'attributes' => array('target' => '_blank'))
    ),
  );
  $form['merchant']['merchant_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant code'),
    '#size' => 30,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_ipay88_merchant_code'),
    '#description' => t('Merchant code assigned by IPay88.'),
  );
  $form['merchant']['merchant_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant key'),
    '#size' => 30,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_ipay88_merchant_key'),
    '#description' => t('Merchant\'s private key.'),
  );

  if (class_exists('IPay88')) {
    $payment_methods = _commerce_ipay88_get_payment_methods();
    $form['payment_methods'] = array(
      '#type' => 'fieldset',
      '#title' => t('Payment methods'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['payment_methods']['payment_methods_enabled'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled payment methods'),
      '#default_value' => variable_get('commerce_ipay88_enabled_payment_methods', array(2, 6, 8, 10, 14, 15, 16, 17, 20, 21, 23, 48)),  // Enabled common payment methods by default.
      '#options' => $payment_methods,
      '#required' => TRUE,
      '#description' => t('Select the payments to be enabled on checkout.'),
    );
  }
  else {
    drupal_set_message(t('Class <em>IPay88</em> not found. Please make sure you have followed setup instructions in Readme file.'), 'error');
  }

  $form['requery'] = array(
    '#type' => 'fieldset',
    '#title' => t('Re-query settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['requery']['requery_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable re-query'),
    '#default_value' => variable_get('commerce_ipay88_cron_requery_enabled', FALSE),
  );
  $form['requery']['requery_cron_limit'] = array(
    '#type' => 'select',
    '#title' => t('Cron limit'),
    '#default_value' => variable_get('commerce_ipay88_cron_requery_limit', 50),
    '#options' => array(25 => 25, 50 => 50, 100 => 100, 200 => 200, 300 => 300, 0 => t('No limit')),
    '#required' => TRUE,
    '#description' => t('Specify the number of transaction to validate / requery on cron run.'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

function commerce_ipay88_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  // Save settings into variable table.
  variable_set('commerce_ipay88_merchant_code', strtoupper($values['merchant_code']));
  variable_set('commerce_ipay88_merchant_key', $values['merchant_key']);
  variable_set('commerce_ipay88_enabled_payment_methods', isset($values['payment_methods_enabled']) ? $values['payment_methods_enabled'] : array());
  variable_set('commerce_ipay88_cron_requery_enabled', $values['requery_enabled']);
  variable_set('commerce_ipay88_cron_requery_limit', $values['requery_cron_limit']);

  drupal_set_message(t('Settings has been saved.'));
}

/**
 * Implements hook_commerce_payment_method_info().
 *
 * @todo Allow terminal payment.
 */
function commerce_ipay88_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_ipay88'] = array(
    'title' => t('IPay88'),
    'description' => t('Pay with IPay88 payment method.'),
    'terminal' => FALSE,
    'active' => TRUE,
    'offsite' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: submit form.
 */
function commerce_ipay88_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  // Unset all previous orders from session.
  drupal_session_start();
  unset($_SESSION['commerce_ipay88']);

  $form = array();

  // Merge in values from the order.
  if (!empty($order->data['commerce_ipay88'])) {
    $pane_values += $order->data['commerce_ipay88'];
  }

  // Merge in default values.
  $pane_values += array(
    'payment_id' => 2,  // Default to Credit Card (MYR).
  );

  $form['ipay88_logo'] = array(
    '#markup' => l(
      theme_image(array(
        'path' => drupal_get_path('module', 'commerce_ipay88') . '/images/ipay88-logo.png',
        'alt' => t('IPay88 - Internet Payment by Mobile88.com.'),
        'title' => t('IPay88 - Internet Payment by Mobile88.com.'),
        'attributes' => array('style' => 'float: right; margin: 0 1em 1em 0;'),
      )),
      'http://www.ipay88.com',
      array('html' => TRUE, 'attributes' => array('target' => '_blank'))
    ),
  );

  // Fetch list of enabled payment methods from configuration.
  $enabled_payment_methods = _commerce_ipay88_get_payment_methods();
  $payment_methods = array();
  if ($enabled_payment_methods) {
    foreach ($enabled_payment_methods as $key => $val) {
      if (in_array($key, variable_get('commerce_ipay88_enabled_payment_methods', array()))) {
        $payment_methods[$key] = $val;
      }
    }
  }
  if (!$payment_methods) {
    drupal_set_message(t('There are no available payment methods configured.'), 'warning');
  }
  $form['payment_id'] = array(
    '#type' => 'select',
    '#title' => t('Payment method'),
    '#default_value' => $pane_values['payment_id'],
    '#options' => $payment_methods,
    '#required' => TRUE,
    '#description' => t('Choose the payment method to use. You can still change this in IPay88 payment page.'),
  );

  return $form;
}

/**
 * On submission form, store selected payment ID in session temporarily.
 */
function commerce_ipay88_submit_form_submit($form, &$form_state) {
  drupal_session_start();
  $_SESSION['commerce_ipay88']['payment_id'] = $form_state['payment_id']['#value'];
}

/**
 * Implements hook_redirect_form().
 */
function commerce_ipay88_redirect_form($form, &$form_state, $order, $payment_method) {
  // Return error if merchant code and key is not configured.
  if (variable_get('commerce_ipay88_merchant_code') == '') {
    drupal_set_message(t('Merchant Code is not configured for IPay88 yet. Please !click_here to review configuration.', array('!click_here' => l('click here', 'admin/commerce/config/ipay88'))), 'error');
    return array();
  }
  if (variable_get('commerce_ipay88_merchant_key') == '') {
    drupal_set_message(t('Merchant Key is not configured for IPay88 yet. Please !click_here to review configuration.', array('!click_here' => l('click here', 'admin/commerce/config/ipay88'))), 'error');
    return array();
  }

  $settings = array(
    // Return to the previous page when payment is canceled.
    'cancel_return' => url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
    // Return to the payment redirect page for processing successful payments.
    'return' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
    // Specify the current payment method instance ID.
    'payment_method' => $payment_method['instance_id'],
  );

  return commerce_ipay88_order_form($form, $form_state, $order, $payment_method['settings'] + $settings);
}

function commerce_ipay88_order_form($form, &$form_state, $order, $settings) {
  // Add Javascript to automatically post to IPay88, and hide the submit button to prevent multiple submission.
  drupal_add_js('jQuery(function ($) { $(\'#commerce-ipay88-redirect-form .form-submit\').hide(); $(\'#commerce-ipay88-redirect-form\').submit(); } );', 'inline');

  drupal_session_start();
  $payment_id = isset($_SESSION['commerce_ipay88']['payment_id']) ? $_SESSION['commerce_ipay88']['payment_id'] : 0;

  // Ensure a default value for the payment_method setting.
  $settings += array('payment_method' => '');

  // Load customer profile.
  $customer_profile = commerce_customer_profile_load($order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id']);
  $user_profile = user_load($customer_profile->uid);

  // Build the data array that will be converted into hidden form values.
  $ipay88 = new IPay88(variable_get('commerce_ipay88_merchant_code'));
  $ipay88->setMerchantKey(variable_get('commerce_ipay88_merchant_key'));
  $ipay88->setField('PaymentId', (int) $payment_id);
  $ipay88->setField('RefNo', strtoupper(substr(md5($customer_profile->profile_id . $customer_profile->uid . $order->order_id . $order->created . $order->revision_id . REQUEST_TIME), 0, 20)));
  $ipay88->setField('Amount', number_format(commerce_currency_amount_to_decimal(100, 'MYR'), 2));
  $ipay88->setField('Currency', $order->commerce_order_total['und'][0]['currency_code']);
  $ipay88->setField('ProdDesc', substr(variable_get('site_name', 'Drupal Commerce') . ' - Order #' . $order->order_id, 0, 100));
  $ipay88->setField('UserName', $user_profile->name);
  $ipay88->setField('UserEmail', $user_profile->mail);
  //$ipay88->setField('UserContact', '');
  //$ipay88->setField('Remark', '');
  //$ipay88->setField('Lang', 'utf-8');
  $ipay88->setField('ResponseURL', $settings['return']);
  $ipay88->generateSignature();
  $data = $ipay88->getFields();

  // Temporarily store data in session (this is not used for validating payment, just logging)
  drupal_session_start();
  unset($_SESSION['commerce_ipay88']['orders']);  // Unset all previous orders from session.
  $_SESSION['commerce_ipay88']['orders'][$order->order_id] = serialize($data);

  $form['redirect'] = array(
    '#markup' => '<br />' . t('<p>You are being redirected to IPay88 payment page. If not, click on the button below.</p>'),
  );

  $form['#action'] = IPay88::$epayment_url;

  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to IPay88'),
  );

  return $form;
}

/**
 * Implements hook_redirect_form_validate.
 */
function commerce_ipay88_redirect_form_validate($order, $payment_method) {
  // Receive response from IPay88 server.
  $ipay88 = new IPay88(variable_get('commerce_ipay88_merchant_code'));
  $response = $ipay88->getResponse();

  drupal_session_start();
  $ipay88_session_temp = isset($_SESSION['commerce_ipay88']['orders'][$order->order_id]) ? unserialize($_SESSION['commerce_ipay88']['orders'][$order->order_id]) : array();
  unset($_SESSION['commerce_ipay88']['orders'][$order->order_id]);

  // Log message to watchdog.
  if (!$response['status']) {
    watchdog('commerce_ipay88', t('Transaction was not successful for Order ID !order_id with the error:- @err_msg'), array('!order_id' => $order->order_id, '@err_msg' => $response['message']), WATCHDOG_WARNING);
    return FALSE;
  }
  else {
    watchdog('commerce_ipay88', t('Transaction was successful for Order ID !order_id.'), array('!order_id' => $order->order_id));

    commerce_ipay88_transaction($payment_method, $order, $response['data'] + $ipay88_session_temp);
  }

  return TRUE;
}

/**
 * Create a transaction and associate it with an order.
 */
function commerce_ipay88_transaction($payment_method, $order, $response) {
  $transaction = commerce_payment_transaction_new('commerce_ipay88', $order->order_id);

  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = str_replace('.', '', str_replace(',', '', $response['Amount']));
  $transaction->currency_code = $response['Currency'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = 'MerchantCode @merchant_code. PaymentId=@payment_id. Amount=@amount. Currency=@currency. Signature=@signature';
  $transaction->message_variables =
    array(
      '@merchant_code' => $response['MerchantCode'],
      '@payment_id' => $response['PaymentId'],
      '@ref_no' => $response['RefNo'],
      '@amount' => $response['Amount'],
      '@currency' => $response['Currency'],
      '@signature' => $response['Signature'],
  );
  commerce_payment_transaction_save($transaction);

  // Insert to IPay88's table for logging.
  if ($transaction->transaction_id) {
    $commerce_ipay88_nid = db_insert('commerce_ipay88')
      ->fields(array(
        'merchant_code' => $response['MerchantCode'],
        'payment_id' => $response['PaymentId'],
        'ref_no' => $response['RefNo'],
        'amount' => str_replace('.', '', str_replace(',', '', $response['Amount'])),
        'currency' => $response['Currency'],
        'prod_desc' => $response['ProdDesc'],
        'user_name' => $response['UserName'],
        'user_email' => $response['UserEmail'],
        'user_contact' => $response['UserContact'],
        'remark' => $response['Remark'],
        'lang' => $response['Lang'],
        'signature' => $response['Signature'],
        'order_id' => $order->order_id,
        'transaction_id' => $transaction->transaction_id,
        'requery_status' => 0,
        'requery_timestamp' => 0,
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
      ))
      ->execute();
  }
}

/**
 * Get all available payment methods as defined from its class.
 *
 * @param boolean $sort Set to TRUE to return payment methods that are sorted.
 *
 * @return array Available payment methods.
 */
function _commerce_ipay88_get_payment_methods($sort = TRUE) {
  $payment_methods = IPay88::$payment_methods;
  $payment_methods_formatted = array();
  if ($payment_methods) {
//    // Whitelist payment methods to defined options here.
//    foreach ($payment_methods as $key => $payment_method) {
//      if (!in_array($payment_method[0], array(2, 6, 8, 10, 14, 15, 16, 17, 20, 21, 23, 48))) {
//        unset($payment_methods[$key]);
//      }
//    }

    // Format result to be presented.
    foreach ($payment_methods as $payment_method) {
      if (isset($payment_method[0]) && isset($payment_method[1])) {
        $payment_methods_formatted[$payment_method[0]] = trim($payment_method[1]) . (isset($payment_method[2]) ? ' (' . strtoupper(trim($payment_method[2])) .')' : '');
      }
    }
  }
  asort($payment_methods_formatted);

  return $payment_methods_formatted;
}