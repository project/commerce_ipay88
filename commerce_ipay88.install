<?php

/**
 * @file
 * Requirements check and defining database table schema.
 */

/**
 * Implementation of hook_requirements().
 *
 * Check if cURL library is installed.
 */
function commerce_ipay88_requirements($phase) {
  // Skip the requirements check if SimpleTest is installed to avoid multiple cURL rows.
  if (module_exists('simpletest')) {
    return;
  }

  $t = get_t();

  $has_curl = function_exists('curl_init');

  $requirements['commerce_ipay88_curl'] = array(
    'title' => $t('cURL'),
    'value' => $has_curl ? $t('Enabled') : $t('Not found'),
  );

  if (!$has_curl) {
    $requirements['commerce_ipay88_curl'] += array(
      'severity' => REQUIREMENT_ERROR,
      'description' => $t("IPay88 requires the PHP <a href='!curl_url'>cURL</a> library.", array('!curl_url' => 'http://php.net/manual/en/curl.setup.php')),
    );
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function commerce_ipay88_schema() {
  $schema = array();

  $schema['commerce_ipay88'] = array(
    'description' => 'Stores processed transactions.',
    'fields' => array(
      'merchant_code' => array(
        'description' => 'Merchant code assigned by IPay88.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'payment_id' => array(
        'description' => 'IPay88 payment ID.',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'ref_no' => array(
        'description' => 'Unique merchant transaction number or usually order ID.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'amount' => array(
        'description' => 'Payment amount.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'currency' => array(
        'description' => 'Currency.',
        'type' => 'varchar',
        'length' => 5,
        'not null' => TRUE,
      ),
      'prod_desc' => array(
        'description' => 'Product description.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'user_name' => array(
        'description' => 'Customer name.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'user_email' => array(
        'description' => 'Customer email.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'user_contact' => array(
        'description' => 'Customer contact.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'remark' => array(
        'description' => 'Merchant remarks.',
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'lang' => array(
        'description' => 'Encoding type.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ),
      'signature' => array(
        'description' => 'SHA1 signature.',
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
      ),
      'order_id' => array(
        'description' => 'The order ID the payment belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'transaction_id' => array(
        'description' => 'The payment transaction ID the payment belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'requery_status' => array(
        'description' => 'Flag whether this transaction has been \'requeried\' with IPay88 to check its status.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'requery_timestamp' => array(
        'description' => 'The Unix timestamp when the requery was called.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the record was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('ref_no'),
    'foreign keys' => array(
      'order_id' => array(
        'table' => 'commerce_order',
        'columns'=> array('order_id' => 'order_id'),
      ),
      'transaction_id' => array(
        'table' => 'commerce_payment_transaction',
        'columns'=> array('payment_id' => 'payment_id'),
      ),
    ),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function commerce_ipay88_uninstall() {
  // Perform some clean on uninstall.
  drupal_session_start();
  if (isset($_SESSION['commerce_ipay88'])) {
    unset($_SESSION['commerce_ipay88']);
  }

  variable_del('commerce_ipay88_merchant_code');
  variable_del('commerce_ipay88_merchant_key');
  variable_del('commerce_ipay88_enabled_payment_methods');
}